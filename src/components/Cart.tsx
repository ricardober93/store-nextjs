import { useAppStore } from "@app/lib/slices/store";
import {
  Badge,
  Box,
  Button,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  Flex,
  HStack,
  Input,
  Text,
  VStack,
  useDisclosure,
} from "@chakra-ui/react";
import React from "react";
import { FiShoppingCart } from "react-icons/fi";
import { useRouter } from 'next/router' 

export default function Cart() {
  const { cart, updateQuantity } = useAppStore();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const router = useRouter();
  const btnRef = React.useRef(null);
  return (
    <>
      <Flex position={"relative"} justifyContent="center" alignItems="center">
        <Button
          ref={btnRef}
          aria-label="Toggle Color Mode"
          onClick={onOpen}
          _focus={{ boxShadow: "none" }}
          w="fit-content"
        >
          {cart.length === 0 ? null : (
            <Badge
              position={"absolute"}
              top={-1}
              right={-1}
              borderRadius={"full"}
              color={"white"}
              bg="red.600"
              fontSize={"0.6em"}
            >
              {cart.length}
            </Badge>
          )}

          <FiShoppingCart />
        </Button>
      </Flex>

      <Drawer
        isOpen={isOpen}
        placement="right"
        onClose={onClose}
        finalFocusRef={btnRef}
      >
        <DrawerOverlay />
        <DrawerContent>
          <DrawerCloseButton />
          <DrawerHeader>Lo que tienes en tu carrito</DrawerHeader>

          <DrawerBody flexDir={"column"}>
            <VStack w={"100%"} gap={4}>
              {cart.length !== 0 ? (
                cart.map((product) => {
                  return (
                    <Flex
                      flex={"1"}
                      w={"100%"}
                      direction={"column"}
                      key={product.id}
                    >
                      <Text>{product.title}</Text>
                      <Text>{product.price * product.quantity!}</Text>
                      <Box>
                        <HStack maxW="140px">
                          <Button
                            onClick={() =>
                              updateQuantity(product.id, "increase")
                            }
                          >
                            +
                          </Button>
                          <Input type="number" value={product.quantity} />
                          <Button
                            onClick={() =>
                              updateQuantity(product.id, "decrease")
                            }
                          >
                            -
                          </Button>
                        </HStack>
                      </Box>
                    </Flex>
                  );
                })
              ) : (
                <Text>No hay productos en el carrito</Text>
              )}
            </VStack>
          </DrawerBody>

          <DrawerFooter>
            <VStack w={"100%"}>
              <Flex w={"100%"} justify={"space-between"} align={"center"}>
                <Text>Total</Text>
                <Text fontSize={"2xl"} fontWeight={"bold"}>
                  {cart.reduce(
                    (acc, product) => acc + product.price * product.quantity!,
                    0
                  )}
                </Text>
              </Flex>

              <Button 
              onClick={() => router.push('/checkout')} 
              w={"100%"} colorScheme="blue">
                Comprar
              </Button>
              <Button w={"100%"} variant="outline" onClick={onClose}>
                {" "}
                Cerrar{" "}
              </Button>
            </VStack>
          </DrawerFooter>
        </DrawerContent>
      </Drawer>
    </>
  );
}
