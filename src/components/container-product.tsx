import { type Product } from "@app/models/Product";
import { Container, SimpleGrid } from "@chakra-ui/react";
import CardEcommerce from "./ui/card-ecommerce";
import { useEffect, useState } from "react";

export default function ContainerProducts() {
  const [products, setProduct] = useState<Product[]>([]);

  useEffect(() => {
    const getProducts = async () => {
      try {
        const response = await fetch("/api/product/get-product");
        const data = await response.json();
        setProduct(data.products);
      } catch (error) {
        console.log(error);
      }
    };

    getProducts();
  }, []);

  if (products?.length === 0) {
    return null;
  }

  return (
    <Container flex={1} maxW="container.lg" py={12}>
      <SimpleGrid minChildWidth="300px" spacing="40px">
        {products?.map((product) => {
          return <CardEcommerce key={product.id} product={product} />;
        })}
      </SimpleGrid>
    </Container>
  );
}
