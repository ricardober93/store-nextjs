import { useAppStore } from "@app/lib/slices/store";
import type { Product } from "@app/models/Product";
import {
  Badge,
  Box,
  Circle,
  Flex,
  Icon,
  Image,
  Tooltip,
  useColorModeValue,
} from "@chakra-ui/react";
import Link from "next/link";
import { FiShoppingCart } from "react-icons/fi";
import Rating from "./Rating";

function CardEcommerce({ product }: { product: Product }) {
  const { addToCart } = useAppStore();

  if (product === undefined) {
    return null;
  }

  return (
    <Flex as="article" w="full" alignItems="center" justifyContent="center">
      <Box
        // eslint-disable-next-line react-hooks/rules-of-hooks
        bg={useColorModeValue("white", "gray.800")}
        maxW="sm"
        borderWidth="1px"
        rounded="lg"
        shadow="lg"
        position="relative"
      >
        {product?.isNew && (
          <Circle
            size="10px"
            position="absolute"
            top={2}
            right={2}
            bg="red.200"
          />
        )}

        <Link href={`/product/${product?.id}`}>
          <Image
            src={product?.images.url ?? "./default.png"}
            alt={`Picture of ${product?.title}`}
            roundedTop="lg"
          />
        </Link>
        <Box p="6">
          <Box display="flex" alignItems="baseline">
            {product?.isNew && (
              <Badge rounded="full" px="2" fontSize="0.8em" colorScheme="red">
                New
              </Badge>
            )}
          </Box>
          <Flex mt="1" justifyContent="space-between" alignContent="center">
            <Link href={`/product/${product?.id}`}>
              <Box
                fontSize="2xl"
                fontWeight="semibold"
                as="h4"
                lineHeight="tight"
                isTruncated
              >
                {product?.title}
              </Box>
            </Link>
            <Tooltip
              label="Add to cart"
              bg="white"
              placement={"top"}
              color={"gray.800"}
              fontSize={"1.2em"}
            >
              <Icon
                cursor={"pointer"}
                onClick={() => addToCart(product)}
                as={FiShoppingCart}
                h={7}
                w={7}
                alignSelf={"center"}
              />
            </Tooltip>
          </Flex>

          <Flex justifyContent="space-between" alignContent="center">
            <Rating rating={product?.rate} numReviews={product?.reviews} />
            <Box
              fontSize="2xl"
              // eslint-disable-next-line react-hooks/rules-of-hooks
              color={useColorModeValue("gray.800", "white")}
            >
              <Box as="span" color={"gray.600"} fontSize="lg">
                £
              </Box>
              {product?.price}
            </Box>
          </Flex>
        </Box>
      </Box>
    </Flex>
  );
}

export default CardEcommerce;
