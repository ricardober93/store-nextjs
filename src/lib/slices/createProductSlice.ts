import { type Product } from "@prisma/client";
import { type StateCreator } from "zustand";


export interface ProductSlice {
    products: Product[];
    fetchProducts: () => void;
}

export const createProductSlice: StateCreator<ProductSlice> = (set) => ({
    products: [],
    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    fetchProducts: async () => {
        const res = await fetch("/api/products/get-products");
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        set({ products: await res.json() })
    },
})