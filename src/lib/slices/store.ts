
import { create } from 'zustand'
import type { ProductSlice } from './createProductSlice';
import { createProductSlice } from './createProductSlice';
import type { Cart } from '@app/models/Cart';
import { createCartSlice } from './createCartSlice';


type StoreState = ProductSlice & Cart

export const useAppStore = create<StoreState>()((...a) => ({
    ...createProductSlice(...a),
    ...createCartSlice(...a),
}))