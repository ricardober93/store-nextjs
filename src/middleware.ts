
import { getToken } from "next-auth/jwt";
import type { NextRequest  } from "next/server";
import { NextResponse } from "next/server";

export default async function middleware(req: NextRequest) {

  const path = req.nextUrl.pathname;

  if (path === "/auth/login" || path === "/auth/register") {
    return NextResponse.next();
  }

  const session = await getToken({
    req,
    secret: process?.env?.NEXTAUTH_SECRET,
  });
  
  if (!session && path === "/") {
    return NextResponse.redirect(new URL("/auth/login", req.url));
  } else if (session && (path === "/auth/login" || path === "/auth/register")) {
    return NextResponse.redirect(new URL("/", req.url));
  }
  return NextResponse.next();
};