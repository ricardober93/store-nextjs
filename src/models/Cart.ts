import type { Product } from "./Product";


export interface Cart {
    cart: Product[];
    addToCart: (product: Product) => void;
    removeFromCart: (productId: number) => void;
    updateQuantity: (productId: number, action: 'increase' | 'decrease') => void;
    showCart: boolean;
    toggleCart: () => void;
}