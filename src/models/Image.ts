export interface Image {
  id: number;
  image: string;
  url: string;
}
