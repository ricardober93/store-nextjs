import type { Category } from "./Category";
import type { Image } from "./Image";

export interface Product {
    category: Category;
    description: string;
    id: number;
    images: Image;
    price: number;
    title: string;
    quantity?: number;
    reviews: number;
    rate: number;
    isNew: boolean;
}