import { type Session } from "next-auth";
import { SessionProvider } from "next-auth/react";
import { type AppType } from "next/app";
import "@app/styles/globals.css";
import { ChakraProvider } from "@chakra-ui/react";
import { Suspense } from "react";
import WithSubnavigation from "@app/components/nabvar";

const MyApp: AppType<{ session: Session | null }> = ({
  Component,
  pageProps: { session, ...pageProps },
}) => {
  return (
    <SessionProvider session={session}>
      <ChakraProvider>
        <Suspense fallback={null}>
        <WithSubnavigation />
        <Component {...pageProps} />
        </Suspense>
      </ChakraProvider>
    </SessionProvider>
  );
};

export default MyApp;
