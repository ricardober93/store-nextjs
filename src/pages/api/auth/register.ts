import { prisma } from "@app/server/db";
import type { NextApiRequest, NextApiResponse } from "next";
import { hash } from "bcrypt";
import type { Login } from "@app/models/user-login";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { firstName, lastName, email, password } = req.body as Login;

  const exist = await prisma.user.findUnique({
    where: {
      email: email,
    },
  });

  if (exist) {
    res.status(400).json({
      message: "User already exist",
    });
  } else {
    await prisma.user.create({
      data: {
        name: firstName + " " + lastName,
        email: email,
        password: await hash(password, 10),
      },
    });

    res.status(200).json({
      message: "User created",
    });
  }
}
