import { prisma } from "@app/server/db";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  const product = await prisma.product.findFirst({
    where: {
      id: Number(req.query?.id)
    },
    select: {
      id: true,
      title: true,
      price: true,
      description: true,
      quantity: true,
      rate: true,
      categoryId: true,
      images: true,
     },
    take: 10
  });
  res.status(200).json({
    product
  })
}