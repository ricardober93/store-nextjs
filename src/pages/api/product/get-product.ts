import { prisma } from "@app/server/db";
import  type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  const products = await prisma.product.findMany({
    select: {
      id: true,
      title: true,
      price: true,
      quantity: true,
      rate: true,
      images: true,
    }
  });
  res.status(200).json({
    products
  })
}