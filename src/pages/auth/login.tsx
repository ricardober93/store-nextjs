import type { Login } from "@app/models/user-login";
import {
  Box,
  Button,
  Checkbox,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Input,
  Stack,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import { signIn } from "next-auth/react";
import Link from "next/link";
import { useRouter } from "next/router";
import { useForm, type SubmitHandler } from "react-hook-form";

export default function SimpleCard() {
  const router = useRouter();
  const { register, handleSubmit } = useForm<Login>();
  const onSubmit: SubmitHandler<Login> = (data) => {
    signIn("credentials", {
      redirect: false,
      email: data.email,
      password: data.password,
    })
      .then(async (response) => {
        if (response?.error) {
        } else {
          await router.push("/");
          console.log("login to home");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <Flex
      minH={"100vh"}
      align={"center"}
      justify={"center"}
      bg={useColorModeValue("gray.50", "gray.800")}
    >
      <Stack spacing={8} mx={"auto"} maxW={"lg"} py={12} px={6}>
        <Stack align={"center"}>
          <Heading fontSize={"4xl"}>Inicia Sesión</Heading>
          <Text fontSize={"lg"} color={"gray.600"}>
            Disfruta de nuestros productos más recientes
          </Text>
        </Stack>
        <Box
          as={"form"}
          onSubmit={handleSubmit(onSubmit)}
          rounded={"lg"}
          bg={useColorModeValue("white", "gray.700")}
          boxShadow={"lg"}
          p={8}
        >
          <Stack spacing={4}>
            <FormControl id="email">
              <FormLabel>Email address</FormLabel>
              <Input {...register("email")} type="email" />
            </FormControl>
            <FormControl id="password">
              <FormLabel>Password</FormLabel>
              <Input {...register("password")} type="password" />
            </FormControl>
            <Stack spacing={10}>
              <Stack
                direction={{ base: "column", sm: "row" }}
                align={"start"}
                justify={"space-between"}
              >
                <Checkbox>Remember me</Checkbox>
                <Text color={"blue.400"}>Forgot password?</Text>
              </Stack>
              <Button
                type="submit"
                bg={"blue.400"}
                color={"white"}
                _hover={{
                  bg: "blue.500",
                }}
              >
                Sign in
              </Button>
            </Stack>
          </Stack>
        </Box>
        <Box>
          <Text align={"center"}>
            No tienes una cuenta?{" "}
            <Link href={"/auth/register"}>
              <Text as="span" color={"blue.400"}>
                Registrate
              </Text>{" "}
            </Link>
          </Text>
        </Box>
      </Stack>
    </Flex>
  );
}
