import { PrismaAdapter } from "@next-auth/prisma-adapter";
import { type GetServerSidePropsContext } from "next";
import {
  getServerSession,
  type NextAuthOptions,
  type DefaultSession,
} from "next-auth";
import { env } from "@app/env.mjs";
import { prisma } from "@app/server/db";
import { compare } from "bcrypt";
import CredentialsProvider from "next-auth/providers/credentials"
import type { Role } from "@prisma/client";

declare module "next-auth" {
  interface Session extends DefaultSession {
    user: DefaultSession["user"] & {
      id: string;
      // ...other properties
      role: Role;
    };
  }

  interface User {
    id: string;
    role: Role;
  }
}

export const authOptions: NextAuthOptions = {
  adapter: PrismaAdapter(prisma),
  providers: [
    CredentialsProvider({
      name: 'credentials',
      credentials: {
        email: { label: "Email", type: "email" },
        password: { label: "Password", type: "password" },
      },
      async authorize(credentials) {

        if (!credentials?.email || !credentials?.password) {
          throw new Error("Missing username or password");
        }
        const user = await prisma.user.findUnique({
          where: {
            email: credentials.email
          },
        });
        if (!user || !(await compare(credentials.password, user.password!))) {
          throw new Error("Invalid username or password");
        }
        return user;
      },
    }),
  ],
  secret: env.NEXTAUTH_SECRET,
  callbacks: {
    session:  ({ session, token }) => {
      return ({
        ...session,
        user: {
          ...session?.user,
          id: token?.sub,
          role: token?.role
        },
      })
    },
  },
  session: {
    strategy: "jwt",
  },
};

/**
 * Wrapper for `getServerSession` so that you don't need to import the `authOptions` in every file.
 *
 * @see https://next-auth.js.org/configuration/nextjs
 */
export const getServerAuthSession = (ctx: {
  req: GetServerSidePropsContext["req"];
  res: GetServerSidePropsContext["res"];
}) => {
  return getServerSession(ctx.req, ctx.res, authOptions);
};
